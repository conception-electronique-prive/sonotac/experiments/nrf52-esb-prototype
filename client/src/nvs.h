#ifndef CEP_NRF_ESB_ADDRESS_CONFIGURATION_NVM_H
#define CEP_NRF_ESB_ADDRESS_CONFIGURATION_NVM_H

#include <stdbool.h>
#include <stdint.h>

int nvs_save(uint32_t address, uint8_t* word);
int nvs_load(uint32_t address, uint8_t* word);
int nvs_erase(uint32_t address);

#endif
