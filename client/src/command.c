#include "command.h"

const char* commandToStr(enum command_t command) {
  switch (command) {
    case COMMAND_NOP: return "NOP"; break;
    case COMMAND_INIT: return "INIT";
    case COMMAND_SUSPEND: return "SUSPEND";
    case COMMAND_DISABLE: return "DISABLE";
    case COMMAND_IS_IDLE: return "IS_IDLE";
    case COMMAND_WRITE_PAYLOAD: return "WRITE_PAYLOAD";
    case COMMAND_READ_RX_PAYLOAD: return "READ_RX_PAYLOAD";
    case COMMAND_START_TX: return "START_TX";
    case COMMAND_START_RX: return "START_RX";
    case COMMAND_STOP_RX: return "STOP_RX";
    case COMMAND_FLUSH_TX: return "FLUSH_TX";
    case COMMAND_POP_TX: return "POP_TX";
    case COMMAND_TX_FULL: return "TX_FULL";
    case COMMAND_FLUSH_RX: return "FLUSH_RX";
    case COMMAND_SET_ADDRESS_LENGTH: return "SET_ADDRESS_LENGTH";
    case COMMAND_SET_BASE_ADDRESS_0: return "SET_BASE_ADDRESS_0";
    case COMMAND_SET_BASE_ADDRESS_1: return "SET_BASE_ADDRESS_1";
    case COMMAND_SET_PREFIXES: return "SET_PREFIXES";
    case COMMAND_ENABLE_PIPES: return "ENABLE_PIPES";
    case COMMAND_UPDATE_PREFIX: return "UPDATE_PREFIX";
    case COMMAND_SET_RF_CHANNEL: return "SET_RF_CHANNEL";
    case COMMAND_GET_RF_CHANNEL: return "GET_RF_CHANNEL";
    case COMMAND_SET_TX_POWER: return "SET_TX_POWER";
    case COMMAND_SET_RETRANSMIT_DELAY: return "SET_RETRANSMIT_DELAY";
    case COMMAND_SET_RETRANSMIT_COUNT: return "SET_RETRANSMIT_COUNT";
    case COMMAND_SET_BITRATE: return "SET_BITRATE";
    case COMMAND_REUSE_PID: return "REUSE_PID";
    case COMMAND_GET_LAST_EVT: return "GET_LAST_EVT";
    case COMMAND_SAVE_WORD: return "SAVE_WORD";
    case COMMAND_LOAD_WORD: return "LOAD_WORD";
    case COMMAND_ERASE_WORD: return "ERASE_WORD";
    default: return "Unknown command";
  }
}
