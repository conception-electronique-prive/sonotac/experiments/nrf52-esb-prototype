#include "nvs.h"

#include <nrfx_nvmc.h>

static bool nvs_is_valid(uint8_t *check1, uint8_t *check2) {
  return check1[0] == check2[0]  //
      && check1[1] == check2[1]  //
      && check1[2] == check2[2]  //
      && check1[3] == check2[3];
}

static void nvs_compute_check(uint8_t *word, uint8_t *check) {
  check[0] = 0xCA ^ word[0];
  check[1] = 0xFE ^ word[1];
  check[2] = 0xFA ^ word[2];
  check[3] = 0xDE ^ word[3];
}

static bool nvs_is_address_valid(uint32_t address) {
  // odd address is for check
  return (address & 1) == 0;
}

int nvs_load(uint32_t address, uint8_t *word) {
  if (!nvs_is_address_valid(address)) return -1;
  uint8_t   check[4];
  uint32_t *data = (uint32_t *)address;
  word[0]        = (data[0] >> 0) & 0xFF;
  word[1]        = (data[0] >> 8) & 0xFF;
  word[2]        = (data[0] >> 16) & 0xFF;
  word[3]        = (data[0] >> 24) & 0xFF;
  check[0]       = (data[4] >> 0) & 0xFF;
  check[1]       = (data[4] >> 8) & 0xFF;
  check[2]       = (data[4] >> 16) & 0xFF;
  check[3]       = (data[4] >> 24) & 0xFF;
  uint8_t check2[4];
  nvs_compute_check(word, check2);
  if (!nvs_is_valid(check, check2)) return -1;
  return 0;
}

int nvs_save(uint32_t address, uint8_t *word) {
  if (!nvs_is_address_valid(address)) return -1;
  uint8_t check[4];
  nvs_compute_check(word, check);
  uint8_t data[8] = { word[0], word[1], word[2], word[3], check[0], check[1], check[2], check[3] };
  nrfx_nvmc_bytes_write(address, data, 8);
  return 0;
}

int nvs_erase(uint32_t address) {
  if (!nvs_is_address_valid(address)) return -1;
  nrfx_nvmc_page_erase(address);
  return 0;
}
