#ifndef CIRCULAR_BUFFER_H
#define CIRCULAR_BUFFER_H

#include <stdbool.h>
#include <stddef.h>

struct circular_buffer;
bool   circular_buffer_create(struct circular_buffer* cb, void* buffer, size_t element_size, size_t capacity);
bool   circular_buffer_push(struct circular_buffer* cb, void* element);
bool   circular_buffer_pop(struct circular_buffer* cb);
bool   circular_buffer_peek(struct circular_buffer* cb, void* element);
bool   circular_buffer_read(struct circular_buffer* cb, void* element);
size_t circular_buffer_size(struct circular_buffer* cb);
size_t circular_buffer_capacity(struct circular_buffer* cb);
bool   circular_buffer_full(struct circular_buffer* cb);
bool   circular_buffer_empty(struct circular_buffer* cb);

#endif
