#ifndef CEP_NRF_SRC_CLIENT_H
#define CEP_NRF_SRC_CLIENT_H

#include <stddef.h>
#include <stdint.h>

#include "command.h"

typedef void (*spi_event_callback_t)(
    enum command_t command,
    const uint8_t *param_buffer,
    size_t         param_length,
    uint8_t       *tx_buffer,
    size_t        *tx_length);

void client_on_spi_event(
    enum command_t command,
    const uint8_t *param_buffer,
    size_t         param_length,
    uint8_t       *tx_buffer,
    size_t        *tx_length);

#endif
