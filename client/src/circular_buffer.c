#include "circular_buffer.h"

#include <string.h>

struct circular_buffer {
  void*  buffer;
  size_t element_size;
  size_t capacity;
  size_t size;
  size_t write_pos;
  size_t read_pos;
};

static size_t increment_cursor(size_t cursor, size_t capacity) { return cursor == capacity - 1 ? 0 : cursor + 1; }

bool circular_buffer_create(struct circular_buffer* cb, void* buffer, size_t element_size, size_t capacity) {
  cb->buffer       = buffer;
  cb->element_size = element_size;
  cb->capacity     = capacity;
  cb->size         = 0;
  cb->write_pos    = 0;
  cb->read_pos     = 0;
  return true;
}

bool circular_buffer_push(struct circular_buffer* cb, void* element) {
  if (circular_buffer_full(cb)) return false;
  memcpy(cb->buffer + (cb->write_pos * cb->element_size), element, cb->element_size);
  cb->write_pos = increment_cursor(cb->write_pos, cb->capacity);
  cb->size++;
  return true;
}

bool circular_buffer_pop(struct circular_buffer* cb) {
  if (circular_buffer_empty(cb)) return false;
  cb->read_pos = increment_cursor(cb->read_pos, cb->capacity);
  cb->size--;
  return true;
}

bool circular_buffer_peek(struct circular_buffer* cb, void* element) {
  if (circular_buffer_empty(cb)) return false;
  memcpy(element, cb->buffer + (cb->read_pos * cb->element_size), cb->element_size);
  return true;
}

bool circular_buffer_read(struct circular_buffer* cb, void* element) {
  if (!circular_buffer_peek(cb, element)) return false;
  cb->read_pos = increment_cursor(cb->read_pos, cb->capacity);
  cb->size--;
  return true;
}

size_t circular_buffer_size(struct circular_buffer* cb) { return cb->size; }

size_t circular_buffer_capacity(struct circular_buffer* cb) { return cb->capacity; }

bool circular_buffer_full(struct circular_buffer* cb) { }

bool circular_buffer_empty(struct circular_buffer* cb);
