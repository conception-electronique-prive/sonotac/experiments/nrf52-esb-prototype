#include "client.h"

#include <esb.h>
#include <hal/nrf_gpio.h>
#include <nrfx_systick.h>
#include <string.h>
#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>

#include "bool_utils.h"
#include "esb_utils.h"
#include "nvs.h"

LOG_MODULE_REGISTER(CLIENT, LOG_LEVEL_DBG);

#define IRQ_PIN 12

#define HANDLER_PARAMETERS const uint8_t *param_buffer, size_t param_length

typedef void (*command_handler_t)(HANDLER_PARAMETERS);

#define COMMAND_SIZE        1
#define COMMAND_RESULT_SIZE 1
#define PIPE_ARGUMENT_SIZE  1
#define PAYLOAD_HEADER_SIZE 5

// absolute max SPI message is a command to send a message which include the command header, pipe parameter and the
// payload
#define RESPONSE_MAX_LENGTH COMMAND_SIZE + PIPE_ARGUMENT_SIZE + CONFIG_ESB_MAX_PAYLOAD_LENGTH
static uint8_t *response_buffer = NULL;
static size_t  *response_length = NULL;
static uint8_t *last_event_ptr  = NULL;
static size_t   last_event_size = 5;

static void handle_esb_event(const struct esb_evt *event) {
  LOG_DBG("ESB EVENT: %s", esb_event_id_to_str(event->evt_id));
  static uint8_t last_event[5] = {};
  last_event[0]                = event->evt_id;
  last_event_ptr               = last_event;
  nrf_gpio_pin_set(IRQ_PIN);
}

static void client_prepare_response(uint8_t result, size_t object_length) {
  *response_length   = object_length + COMMAND_RESULT_SIZE;
  response_buffer[0] = result;
}

static void client_make_response() { *response_length = 0; }

static void
client_make_response_with_u8_result_and_object(uint8_t result, uint8_t *object_buffer, size_t object_length) {
  client_prepare_response(result, object_length);
  if (object_length != 0) memcpy(response_buffer + 1, object_buffer, object_length);
}

static void client_make_response_with_u8_result(uint8_t result) {
  client_make_response_with_u8_result_and_object(result, NULL, 0);
}

static void
client_make_response_with_bool_result_and_object(bool result, uint8_t *object_buffer, size_t object_length) {
  client_make_response_with_u8_result_and_object((uint8_t)(result ? 1 : 0), object_buffer, object_length);
}

static void client_make_response_with_bool_result(bool result) {
  client_make_response_with_bool_result_and_object(result, NULL, 0);
}

static void client_make_response_with_i_result_and_object(int result, uint8_t *object_buffer, size_t object_length) {
  client_make_response_with_u8_result_and_object((uint8_t)result, object_buffer, object_length);
}

static void client_make_response_with_i_result(int result) {
  client_make_response_with_i_result_and_object(result, NULL, 0);
}

static void handle_nop(HANDLER_PARAMETERS) {};

static void handle_init(HANDLER_PARAMETERS) {
  int result = -NRFX_ERROR_INVALID_LENGTH;
  if (param_length == 13) {
    // parse config
    struct esb_config config = {
      .protocol           = (enum esb_protocol)param_buffer[0],
      .mode               = (enum esb_mode)param_buffer[1],
      .event_handler      = handle_esb_event,
      .bitrate            = (enum esb_bitrate)param_buffer[2],
      .crc                = (enum esb_crc)param_buffer[3],
      .tx_output_power    = param_buffer[4],
      .retransmit_delay   = param_buffer[5] | param_buffer[6] << 8,
      .retransmit_count   = param_buffer[7] | param_buffer[8] << 8,
      .tx_mode            = (enum esb_tx_mode)param_buffer[9],
      .payload_length     = param_buffer[10],
      .selective_auto_ack = param_buffer[11] != 0,
      .use_fast_ramp_up   = param_buffer[12] != 0,
    };
    LOG_DBG(
        "ESB_CONFIG\n"
        "- Protocol:\t%s\n"
        "- Mode:\t\t%s\n"
        "- Bitrate:\t%s\n"
        "- CRC:\t\t%s\n"
        "- TX Power:\t%d dBm\n"
        "- RETX Delay:\t%d\n"
        "- RETX Count:\t%d\n"
        "- TX Mode:\t%s\n"
        "- PL Length:\t%d\n"
        "- Sel AACK:\t%s\n"
        "- FastRampUp:\t%s\n",
        esb_protocol_to_str(config.protocol),
        esb_mode_to_str(config.mode),
        esb_bitrate_to_str(config.bitrate),
        esb_crc_to_str(config.crc),
        config.tx_output_power,
        config.retransmit_delay,
        config.retransmit_count,
        esb_tx_mode_to_str(config.tx_mode),
        config.payload_length,
        bool_to_str(config.selective_auto_ack),
        bool_to_str(config.use_fast_ramp_up));

    result = esb_init(&config);
  }
  client_make_response_with_i_result(result);
};

static void handle_suspend(HANDLER_PARAMETERS) { client_make_response_with_i_result(esb_suspend()); };

static void handle_disable(HANDLER_PARAMETERS) {
  esb_disable();
  client_make_response();
};

static void handle_is_idle(HANDLER_PARAMETERS) { client_make_response_with_bool_result(esb_is_idle()); };

static void handle_write_payload(HANDLER_PARAMETERS) {
  if (param_length < PAYLOAD_HEADER_SIZE) {
    client_make_response_with_i_result(-NRFX_ERROR_INVALID_LENGTH);
  } else {
    struct esb_payload payload = {
      .length = param_buffer[0],
      .pipe   = param_buffer[1],
      .rssi   = param_buffer[2],
      .noack  = param_buffer[3],
      .pid    = param_buffer[4],
    };
    LOG_DBG("Payload: L(%d), P(%d)", payload.length, payload.pipe);
    if (param_length < (PAYLOAD_HEADER_SIZE + payload.length)) {
      client_make_response_with_i_result((int)-NRFX_ERROR_INVALID_LENGTH);
    } else {
      memcpy(payload.data, param_buffer + PAYLOAD_HEADER_SIZE, payload.length);
      LOG_HEXDUMP_DBG(payload.data, payload.length, "Payload data");
      client_make_response_with_i_result(esb_write_payload(&payload));
    }
  }
};

static void handle_read_rx_payload(HANDLER_PARAMETERS) {
  struct esb_payload payload;
  int                result = esb_read_rx_payload(&payload);
  if (result != 0) {
    client_make_response_with_i_result(result);
  } else {
    size_t payload_size = PAYLOAD_HEADER_SIZE + payload.length;
    client_prepare_response(result, payload_size);
    response_buffer[COMMAND_RESULT_SIZE + 0] = payload.length;
    response_buffer[COMMAND_RESULT_SIZE + 1] = payload.pipe;
    response_buffer[COMMAND_RESULT_SIZE + 2] = payload.rssi;
    response_buffer[COMMAND_RESULT_SIZE + 3] = payload.noack;
    response_buffer[COMMAND_RESULT_SIZE + 4] = payload.pid;
    memcpy(response_buffer + COMMAND_RESULT_SIZE + PAYLOAD_HEADER_SIZE, payload.data, payload.length);
    LOG_DBG("Payload: L(%d), P(%d), R(%d)", payload.length, payload.pipe, payload.rssi);
    LOG_HEXDUMP_DBG(payload.data, payload.length, "Payload Data");
  }
};

static void handle_start_tx(HANDLER_PARAMETERS) { client_make_response_with_i_result(esb_start_tx()); };

static void handle_start_rx(HANDLER_PARAMETERS) { client_make_response_with_i_result(esb_start_rx()); };

static void handle_stop_rx(HANDLER_PARAMETERS) { client_make_response_with_i_result(esb_stop_rx()); };

static void handle_flush_tx(HANDLER_PARAMETERS) { client_make_response_with_i_result(esb_flush_tx()); };

static void handle_pop_tx(HANDLER_PARAMETERS) { client_make_response_with_i_result(esb_pop_tx()); };

static void handle_tx_full(HANDLER_PARAMETERS) { client_make_response_with_i_result(esb_tx_full()); };

static void handle_flush_rx(HANDLER_PARAMETERS) { client_make_response_with_i_result(esb_flush_rx()); };

static void handle_set_address_length(HANDLER_PARAMETERS) {
  if (param_length != 1) {
    client_make_response_with_i_result((int)-NRFX_ERROR_INVALID_LENGTH);
  } else {
    LOG_DBG("Address length: %d", param_buffer[0]);
    client_make_response_with_i_result(esb_set_address_length(param_buffer[0]));
  }
};

static void handle_set_base_address_0(HANDLER_PARAMETERS) {
  LOG_DBG("ADDR_0: %x %x %x %x\n", param_buffer[0], param_buffer[1], param_buffer[2], param_buffer[3]);
  client_make_response_with_i_result(esb_set_base_address_0(param_buffer));
};

static void handle_set_base_address_1(HANDLER_PARAMETERS) {
  LOG_DBG("ADDR_1: %x %x %x %x\n", param_buffer[0], param_buffer[1], param_buffer[2], param_buffer[3]);
  client_make_response_with_i_result(esb_set_base_address_1(param_buffer));
};
static void handle_set_prefixes(HANDLER_PARAMETERS) {
  if (param_length == 0) {
    client_make_response_with_i_result((int)-NRFX_ERROR_INVALID_LENGTH);
  } else {
    for (int i = 0; i < param_length - 1; ++i) LOG_DBG("ADDR_PREFIX %d: %x", i, param_buffer[i]);
    int err = esb_set_prefixes(param_buffer, param_buffer[param_length - 1]);
    client_make_response_with_i_result(err);
  }
};

static void handle_enable_pipes(HANDLER_PARAMETERS) {
  if (param_length != 1) {
    client_make_response_with_i_result((int)-NRFX_ERROR_INVALID_LENGTH);
  } else {
    LOG_DBG("Pipes: %x", param_buffer[0]);
    client_make_response_with_i_result(esb_enable_pipes(param_buffer[0]));
  }
};

static void handle_update_prefix(HANDLER_PARAMETERS) {
  if (param_length != 2) {
    client_make_response_with_i_result((int)-NRFX_ERROR_INVALID_LENGTH);
  } else {
    LOG_DBG("Pipe: %d", param_buffer[0]);
    LOG_DBG("Prefix: %d", param_buffer[1]);
    client_make_response_with_i_result(esb_update_prefix(param_buffer[0], param_buffer[1]));
  }
};

static void handle_set_rf_channel(HANDLER_PARAMETERS) {
  if (param_length != 4) {
    client_make_response_with_i_result((int)-NRFX_ERROR_INVALID_LENGTH);
  } else {
    uint32_t channel = param_buffer[0] | param_buffer[1] << 8 | param_buffer[2] << 16 | param_buffer[3] << 24;
    LOG_DBG("RF Channel: %d", channel);
    client_make_response_with_i_result(esb_set_rf_channel(channel));
  }
};

static void handle_get_rf_channel(HANDLER_PARAMETERS) {
  uint32_t channel = 0;
  client_prepare_response(esb_get_rf_channel(&channel), 4);
  LOG_DBG("RF Channel: %d", channel);
  response_buffer[COMMAND_RESULT_SIZE + 0] = channel & 0xff;
  response_buffer[COMMAND_RESULT_SIZE + 1] = (channel >> 8) & 0xff;
  response_buffer[COMMAND_RESULT_SIZE + 2] = (channel >> 16) & 0xff;
  response_buffer[COMMAND_RESULT_SIZE + 3] = (channel >> 24) & 0xff;
};

static void handle_set_tx_power(HANDLER_PARAMETERS) {
  if (param_length != 1) {
    client_make_response_with_i_result(-NRFX_ERROR_INVALID_LENGTH);
  } else {
    LOG_DBG("TX Power: %d", param_buffer[0]);
    client_make_response_with_i_result(esb_set_tx_power(param_buffer[0]));
  }
};

static void handle_set_retransmit_delay(HANDLER_PARAMETERS) {
  if (param_length != 2) {
    client_make_response_with_i_result(-NRFX_ERROR_INVALID_LENGTH);
  } else {
    uint16_t retransmit_delay = param_buffer[0] | param_buffer[1] << 8;
    LOG_DBG("Restransmit Delay: %d", retransmit_delay);
    client_make_response_with_i_result(esb_set_retransmit_delay(retransmit_delay));
  }
};

static void handle_set_retransmit_count(HANDLER_PARAMETERS) {
  if (param_length != 2) {
    client_make_response_with_i_result(-NRFX_ERROR_INVALID_LENGTH);
  } else {
    uint16_t retransmit_count = param_buffer[0] | param_buffer[1] << 8;
    LOG_DBG("Restransmit Count: %d", retransmit_count);
    client_make_response_with_i_result(esb_set_retransmit_count(retransmit_count));
  }
};

static void handle_set_bitrate(HANDLER_PARAMETERS) {
  if (param_length != 1) {
    client_make_response_with_i_result(-NRFX_ERROR_INVALID_LENGTH);
  } else {
    LOG_DBG("ESB Bitrate: %s", esb_bitrate_to_str(param_buffer[0]));
    client_make_response_with_i_result(esb_set_bitrate((enum esb_bitrate)param_buffer[0]));
  }
};

static void handle_reuse_pid(HANDLER_PARAMETERS) {
  if (param_length != 1) {
    client_make_response_with_i_result(NRFX_ERROR_INVALID_LENGTH);
  } else {
    LOG_DBG("Reuse PID: %s", param_buffer[0] == 0 ? "False" : "True");
    client_make_response_with_i_result(esb_reuse_pid(param_buffer[0]));
  }
};

static void handle_get_last_event(HANDLER_PARAMETERS) {
  if (last_event_ptr == NULL) {
    client_make_response_with_i_result(-NRFX_ERROR_INVALID_STATE);
  } else {
    NRFX_CRITICAL_SECTION_ENTER();
    nrf_gpio_pin_clear(IRQ_PIN);
    client_make_response_with_i_result_and_object(NRFX_SUCCESS, last_event_ptr, last_event_size);
    last_event_ptr = NULL;
    NRFX_CRITICAL_SECTION_EXIT();
  }
};

static void handle_save_word(HANDLER_PARAMETERS) {
  if (param_length != 8) {
    client_make_response_with_i_result(-NRFX_ERROR_INVALID_LENGTH);
  } else {
    uint32_t address = param_buffer[0] | param_buffer[1] << 8 | param_buffer[2] << 16 | param_buffer[3] << 24;
    uint8_t  word[4] = { param_buffer[4], param_buffer[5], param_buffer[6], param_buffer[7] };
    LOG_DBG("Address: %08x", address);
    LOG_DBG("Word: %x %x %x %x", word[0], word[1], word[2], word[3]);
    client_make_response_with_i_result(nvs_save(address, word));
  }
}

static void handle_load_word(HANDLER_PARAMETERS) {
  if (param_length != 4) {
    client_make_response_with_i_result(-NRFX_ERROR_INVALID_LENGTH);
  } else {
    uint32_t address = param_buffer[0] | param_buffer[1] << 8 | param_buffer[2] << 16 | param_buffer[3] << 24;
    uint8_t  word[4];
    LOG_DBG("Address: %08x", address);
    int result = nvs_load(address, word);
    if (result != 0) {
      client_make_response_with_i_result(result);
    } else {
      LOG_DBG("Word: %x %x %x %x", word[0], word[1], word[2], word[3]);
      client_make_response_with_i_result_and_object(result, word, 4);
    }
  }
}

static void handle_erase_word(HANDLER_PARAMETERS) {
  if (param_length != 4) {
    client_make_response_with_i_result(-NRFX_ERROR_INVALID_LENGTH);
  } else {
    uint32_t address = param_buffer[0] | param_buffer[1] << 8 | param_buffer[2] << 16 | param_buffer[3] << 24;
    LOG_DBG("Address: %08x", address);
    client_make_response_with_i_result(nvs_erase(address));
  }
}

static command_handler_t handlers[COMMAND_COUNT] = {
  handle_nop,
  handle_init,
  handle_suspend,
  handle_disable,
  handle_is_idle,
  handle_write_payload,
  handle_read_rx_payload,
  handle_start_tx,
  handle_start_rx,
  handle_stop_rx,
  handle_flush_tx,
  handle_pop_tx,
  handle_tx_full,
  handle_flush_rx,
  handle_set_address_length,
  handle_set_base_address_0,
  handle_set_base_address_1,
  handle_set_prefixes,
  handle_enable_pipes,
  handle_update_prefix,
  handle_set_rf_channel,
  handle_get_rf_channel,
  handle_set_tx_power,
  handle_set_retransmit_delay,
  handle_set_retransmit_count,
  handle_set_bitrate,
  handle_reuse_pid,
  handle_get_last_event,
  handle_save_word,
  handle_load_word,
  handle_erase_word,
};

void client_on_spi_event(
    enum command_t command,
    const uint8_t *param_buffer,
    size_t         param_length,
    uint8_t       *tx_buffer,
    size_t        *tx_length) {
  LOG_DBG("%s start", commandToStr(command));
  for (int i = 0; i < param_length; ++i) LOG_DBG("Param %d: %x", i, param_buffer[i]);
  response_buffer = tx_buffer;
  response_length = tx_length;
  handlers[command](param_buffer, param_length);
  LOG_DBG("%s done", commandToStr(command));
  LOG_DBG("Length: %d", *response_length);
  if (response_length == 0) {
    LOG_DBG("%s", "Data: None");
  } else {
    LOG_HEXDUMP_DBG(response_buffer, *response_length, "Data");
  }
  printk("\n");
}
