#include <esb.h>
#include <hal/nrf_clock.h>
#include <nrfx_systick.h>
#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>
#include <zephyr/sys/reboot.h>

#include "check_result.h"
#include "client.h"
#include "esb_configuration.h"
#include "initialization.h"

LOG_MODULE_REGISTER(APP, LOG_LEVEL_DBG);

static volatile bool had_event = false;

int main(void) {
  init_clock();
  LOG_INF("ESB spi module %s %s", __DATE__, __TIME__);

  uint8_t tx_buffer[50] = { 0 };
  size_t  tx_length     = 0;
  uint8_t is_busy       = -EBUSY;
  uint8_t addr_0[]      = { ADDR_0_0, ADDR_0_1, ADDR_0_2, ADDR_0_3 };
  uint8_t prefixes[]    = { ADDR_PREFIX_0, ADDR_PREFIX_1 };

  uint8_t config_params[] = {
    ESB_CONFIG_PROTOCOL,                        // PROTOCOL
    ESB_MODE_PTX,                               // MODE
    ESB_CONFIG_BITRATE,                         // BITRATE
    ESB_CONFIG_CRC,                             // CRC
    ESB_CONFIG_TX_POWER,                        // TX Power
    ESB_CONFIG_RETRANSMIT_DELAY & 0xFF,         // RETRANSMIT DELAY LSB
    (ESB_CONFIG_RETRANSMIT_DELAY >> 8) & 0xFF,  // RETRANSMIT DELAY MSB
    ESB_CONFIG_RETRANSMIT_COUNT & 0xFF,         // RETRANSMIT COUNT LSB
    (ESB_CONFIG_RETRANSMIT_COUNT >> 8) & 0xFF,  // RETRANSMIT COUNT MSB
    ESB_CONFIG_TXMODE,                          // TXMODE
    ESB_CONFIG_PAYLOAD_LENGTH,                  // PAYLOAD LENGTH
    ESB_CONFIG_SELECTIVE_AUTO_ACK,              // SELECTIVE AUTO ACK
    ESB_CONFIG_USE_FAST_RAMP_UP                 // USE FAST RAMP UP
  };
  uint8_t config_length = sizeof(config_params) / sizeof(uint8_t);
  do {
    client_on_spi_event(COMMAND_INIT, config_params, config_length, tx_buffer, &tx_length);
  } while (tx_buffer[0] == is_busy);
  check_result("init", tx_buffer[0]);

  uint8_t addr_prefix_params[] = { 0x64, 0x63, 2 };
  size_t  addr_prefix_length   = sizeof(addr_prefix_params) / sizeof(uint8_t);
  do {
    client_on_spi_event(COMMAND_SET_PREFIXES, addr_prefix_params, addr_prefix_length, tx_buffer, &tx_length);
  } while (tx_buffer[0] == is_busy);
  check_result("set addr prefix", tx_buffer[0]);

  uint8_t addr_0_params[] = { 0x70, 0x75, 0x63, 0x61 };
  size_t  addr_0_length   = sizeof(addr_0_params) / sizeof(uint8_t);
  do {
    client_on_spi_event(COMMAND_SET_BASE_ADDRESS_0, addr_0_params, addr_0_length, tx_buffer, &tx_length);
  } while (tx_buffer[0] == is_busy);
  check_result("set address 0", tx_buffer[0]);

  uint8_t write_payload_params[] = {
    1,  // LENGTH
    0,  // PIPE
    0,  // RSSI
    0,  // NOACK
    0,  // PID
    0,  // DATA: 0
  };
  size_t write_payload_length = sizeof(write_payload_params) / sizeof(uint8_t);

  while (true) {
    client_on_spi_event(COMMAND_FLUSH_TX, NULL, 0, tx_buffer, &tx_length);
    client_on_spi_event(COMMAND_WRITE_PAYLOAD, write_payload_params, write_payload_length, tx_buffer, &tx_length);
    check_result("write payload", tx_buffer[0]);
    while (true) {
      client_on_spi_event(COMMAND_GET_LAST_EVT, NULL, 0, tx_buffer, &tx_length);
      if (-tx_buffer[0] == NRFX_ERROR_INVALID_STATE) continue;
      check_result("get_last_event", tx_buffer[0]);
      break;
    }
  }

  return 0;
}
