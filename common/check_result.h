#ifndef CHECK_RESULT_H
#define CHECK_RESULT_H
#include <stdbool.h>

bool check_result(const char *command, int result);

#endif