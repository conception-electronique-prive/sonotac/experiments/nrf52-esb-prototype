#include "initialization.h"

#include <hal/nrf_gpio.h>
#include <nrfx_uart.h>
#include <zephyr/drivers/clock_control.h>
#include <zephyr/drivers/clock_control/nrf_clock_control.h>
#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>

#include "bool_utils.h"
#include "esb_utils.h"
#include "esb_configuration.h"
#include "check_result.h"

LOG_MODULE_REGISTER(ESB, LOG_LEVEL_DBG);

int init_clock()
{
  int err;
  int res;
  struct onoff_manager *clk_mgr;
  struct onoff_client clk_cli;

  clk_mgr = z_nrf_clock_control_get_onoff(CLOCK_CONTROL_NRF_SUBSYS_HF);
  if (!clk_mgr)
  {
    LOG_ERR("Unable to get the Clock manager");
    return -ENXIO;
  }

  sys_notify_init_spinwait(&clk_cli.notify);

  err = onoff_request(clk_mgr, &clk_cli);
  if (err < 0)
  {
    LOG_ERR("Clock request failed: %d", err);
    return err;
  }

  do
  {
    err = sys_notify_fetch_result(&clk_cli.notify, &res);
    if (!err && res)
    {
      LOG_ERR("Clock could not be started: %d", res);
      return res;
    }
  } while (err);

  LOG_DBG("HF clock started");
  return 0;
}

int init_esb(struct esb_config *config, const uint8_t *addr_0, const uint8_t *addr_1, const uint8_t *addr_prefix)
{
  int err = 0;
  esb_disable();
  config->protocol = ESB_CONFIG_PROTOCOL;
  config->bitrate = ESB_CONFIG_BITRATE;
  config->crc = ESB_CONFIG_CRC;
  config->tx_output_power = ESB_CONFIG_TX_POWER;
  config->retransmit_delay = ESB_CONFIG_RETRANSMIT_DELAY;
  config->tx_mode = ESB_CONFIG_TXMODE;
  config->payload_length = ESB_CONFIG_PAYLOAD_LENGTH;
  config->selective_auto_ack = ESB_CONFIG_SELECTIVE_AUTO_ACK;
  config->use_fast_ramp_up = ESB_CONFIG_USE_FAST_RAMP_UP;
  LOG_DBG(
      "ESB_CONFIG\n"
      "- Protocol:\t%s\n"
      "- Mode:\t\t%s\n"
      "- Bitrate:\t%s\n"
      "- CRC:\t\t%s\n"
      "- TX Power:\t%d dBm\n"
      "- RETX Delay:\t%d\n"
      "- RETX Count:\t%d\n"
      "- TX Mode:\t%s\n"
      "- PL Length:\t%d\n"
      "- Sel AACK:\t%s\n"
      "- FastRampUp:\t%s",
      esb_protocol_to_str(config->protocol),
      esb_mode_to_str(config->mode),
      esb_bitrate_to_str(config->bitrate),
      esb_crc_to_str(config->crc),
      config->tx_output_power,
      config->retransmit_delay,
      config->retransmit_count,
      esb_tx_mode_to_str(config->tx_mode),
      config->payload_length,
      bool_to_str(config->selective_auto_ack),
      bool_to_str(config->use_fast_ramp_up));

  check_result("init", esb_init(config));
  LOG_DBG("SET_RF_CHANNEL: %d", ESB_CONFIG_RF_CHANNEL);
  check_result("set_ref_channel", esb_set_rf_channel(ESB_CONFIG_RF_CHANNEL));
  LOG_DBG("ESB_ADDR_0: %x %x %x %x", addr_0[0], addr_0[1], addr_0[2], addr_0[3]);
  check_result("set_base_address_0", esb_set_base_address_0(addr_0));
  if (addr_1 != NULL)
  {
    LOG_DBG("ESB_ADDR_1: %x %x %x %x", addr_1[0], addr_1[1], addr_1[2], addr_1[3]);
    check_result("set_base_address_1", esb_set_base_address_1(addr_1));
  }
  LOG_DBG("ESB_PREFIX: %x %x", addr_prefix[0], addr_prefix[1]);
  check_result("set_prefixes", esb_set_prefixes(addr_prefix, 2));
  return 0;
}
