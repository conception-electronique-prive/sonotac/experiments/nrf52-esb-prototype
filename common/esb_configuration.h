#ifndef CEP_NRF_ESB_CONFIGURATION_H
#define CEP_NRF_ESB_CONFIGURATION_H

#include <stdint.h>

#define ESB_CONFIG_PROTOCOL           ESB_PROTOCOL_ESB_DPL
#define ESB_CONFIG_BITRATE            ESB_BITRATE_1MBPS
#define ESB_CONFIG_CRC                ESB_CRC_16BIT
#define ESB_CONFIG_TX_POWER           ESB_TX_POWER_4DBM
#define ESB_CONFIG_RETRANSMIT_DELAY   600
#define ESB_CONFIG_RETRANSMIT_COUNT   5
#define ESB_CONFIG_TXMODE             ESB_TXMODE_AUTO
#define ESB_CONFIG_PAYLOAD_LENGTH     5
#define ESB_CONFIG_SELECTIVE_AUTO_ACK true
#define ESB_CONFIG_USE_FAST_RAMP_UP   false
#define ESB_CONFIG_RF_CHANNEL         50

#define ADDR_0_0 0x70
#define ADDR_0_1 0x75
#define ADDR_0_2 0x63
#define ADDR_0_3 0x61

#define ADDR_1_0 0xC2
#define ADDR_1_1 0xC2
#define ADDR_1_2 0xC2
#define ADDR_1_3 0xC2

#define ADDR_PREFIX_0 0x64
#define ADDR_PREFIX_1 0x63
#define ADDR_PREFIX_2 0xC3
#define ADDR_PREFIX_3 0xC4
#define ADDR_PREFIX_4 0xC5
#define ADDR_PREFIX_5 0xC6
#define ADDR_PREFIX_6 0xC7
#define ADDR_PREFIX_7 0xC8

extern const uint8_t addr_0[4];
extern const uint8_t addr_1[4];
extern const uint8_t addr_prefix[8];

#endif