#include "esb_configuration.h"

const uint8_t addr_0[4]      = { ADDR_0_0, ADDR_0_1, ADDR_0_2, ADDR_0_3 };
const uint8_t addr_1[4]      = { ADDR_1_0, ADDR_1_1, ADDR_1_2, ADDR_1_3 };
const uint8_t addr_prefix[8] = { ADDR_PREFIX_0, ADDR_PREFIX_1, ADDR_PREFIX_2, ADDR_PREFIX_3,
                                 ADDR_PREFIX_4, ADDR_PREFIX_5, ADDR_PREFIX_6, ADDR_PREFIX_7 };
