#ifndef CEP_NRF_COMMON_INITIALIZATION_H
#define CEP_NRF_COMMON_INITIALIZATION_H

#include <esb.h>

int init_clock();
int init_uart();
int init_esb(struct esb_config *config, const uint8_t *addr_0, const uint8_t *addr_1, const uint8_t *addr_prefix);

#endif