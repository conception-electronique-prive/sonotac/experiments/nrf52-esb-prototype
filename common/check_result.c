#include "check_result.h"
#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>

LOG_MODULE_REGISTER(RESULT, LOG_LEVEL_DBG);

bool check_result(const char *command, int result)
{
    if (result != 0)
    {
        LOG_ERR("%s failed: %d", command, -result);
        return true;
    }
    else
    {
        LOG_DBG("%s OK", command);
        return false;
    }
}