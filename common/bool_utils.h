#ifndef BOOL_UTILS_H
#define BOOL_UTILS_H

#include <stdbool.h>

static inline const char* bool_to_str(bool value) { return value ? "True" : "False"; }

#endif
