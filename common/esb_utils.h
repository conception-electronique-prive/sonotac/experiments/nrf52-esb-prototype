#ifndef ESB_UTILS_H
#define ESB_UTILS_H
#include <esb.h>

const char* esb_protocol_to_str(enum esb_protocol protocol);
const char* esb_mode_to_str(enum esb_mode mode);
const char* esb_bitrate_to_str(enum esb_bitrate bitrate);
const char* esb_crc_to_str(enum esb_crc crc);
const char* esb_tx_mode_to_str(enum esb_tx_mode mode);
const char* esb_event_id_to_str(enum esb_evt_id id);

#endif