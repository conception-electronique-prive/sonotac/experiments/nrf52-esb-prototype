#include "esb_utils.h"

const char* esb_protocol_to_str(enum esb_protocol protocol) {
  switch (protocol) {
    case ESB_PROTOCOL_ESB: return "ESB";
    case ESB_PROTOCOL_ESB_DPL: return "ESB DPL";
  }
  return "INVALID";
}

const char* esb_mode_to_str(enum esb_mode mode) {
  switch (mode) {
    case ESB_MODE_PTX: return "PTX";
    case ESB_MODE_PRX: return "PRX";
  }
  return "INVALID";
}

const char* esb_bitrate_to_str(enum esb_bitrate bitrate) {
  switch (bitrate) {
    case ESB_BITRATE_1MBPS: return "1MBPS";
    case ESB_BITRATE_2MBPS: return "2MBPS";
    case ESB_BITRATE_1MBPS_BLE: return "1MBPS BLE";
    case ESB_BITRATE_2MBPS_BLE: return "2MBPS BLE";
  }
  return "INVALID";
}

const char* esb_crc_to_str(enum esb_crc crc) {
  switch (crc) {
    case ESB_CRC_16BIT: return "16BIT";
    case ESB_CRC_8BIT: return "8BIT";
    case ESB_CRC_OFF: return "OFF";
  }
  return "INVALID";
}

const char* esb_tx_mode_to_str(enum esb_tx_mode mode) {
  switch (mode) {
    case ESB_TXMODE_AUTO: return "AUTO";
    case ESB_TXMODE_MANUAL: return "MANUAL";
    case ESB_TXMODE_MANUAL_START: return "MANUAL START";
  }
  return "INVALID";
}

const char* esb_event_id_to_str(enum esb_evt_id id) {
  switch (id) {
    case ESB_EVENT_TX_SUCCESS: return "TX SUCCESS";
    case ESB_EVENT_TX_FAILED: return "TX FAILED";
    case ESB_EVENT_RX_RECEIVED: return "RX RECEIVED";
  }
  return "INVALID";
}
