/*
 * Copyright (c) 2018 Nordic Semiconductor ASA
 *
 * SPDX-License-Identifier: LicenseRef-Nordic-5-Clause
 */
#include <zephyr/drivers/clock_control.h>
#include <zephyr/drivers/clock_control/nrf_clock_control.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/irq.h>
#include <zephyr/logging/log.h>
#include <nrf.h>
#include <esb.h>
#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/kernel.h>
#include <zephyr/types.h>
#include <hal/nrf_gpio.h>

LOG_MODULE_REGISTER(APP, LOG_LEVEL_DBG);

static bool ready = true;
static struct esb_payload rx_payload;
static struct esb_payload tx_payload = ESB_CREATE_PAYLOAD(0,
														  0x01, 0x00, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08);

#define _RADIO_SHORTS_COMMON                                       \
	(RADIO_SHORTS_READY_START_Msk | RADIO_SHORTS_END_DISABLE_Msk | \
	 RADIO_SHORTS_ADDRESS_RSSISTART_Msk |                          \
	 RADIO_SHORTS_DISABLED_RSSISTOP_Msk)

#define LED0_PIN 10
#define LED1_PIN 15
#define LED0 NRF_GPIO_PIN_MAP(1, LED0_PIN)
#define LED1 NRF_GPIO_PIN_MAP(1, LED1_PIN)

void led_toggle(uint32_t pin)
{
	nrf_gpio_pin_toggle(pin);
}

void led_on(uint32_t pin)
{
	nrf_gpio_pin_set(pin);
}

void led_off(uint32_t pin)
{
	nrf_gpio_pin_clear(pin);
}

void send_payload()
{
	int err = esb_write_payload(&tx_payload);
	if (err != 0)
	{
		led_off(LED0);
		led_off(LED1);
		LOG_ERR("Failed to write ack payload");
	}
}

void event_handler(struct esb_evt const *event)
{
	switch (event->evt_id)
	{
	case ESB_EVENT_TX_SUCCESS:
		LOG_DBG("TX SUCCESS EVENT");
		led_on(LED0);
		ready = true;
		break;
	case ESB_EVENT_TX_FAILED:
		LOG_DBG("TX FAILED EVENT");
		led_on(LED1);
		ready = true;
		break;
	case ESB_EVENT_RX_RECEIVED:
		while (esb_read_rx_payload(&rx_payload) == 0)
		{
			LOG_DBG("Packet received, len %d : "
					"0x%02x, 0x%02x, 0x%02x, 0x%02x, "
					"0x%02x, 0x%02x, 0x%02x, 0x%02x",
					rx_payload.length, rx_payload.data[0],
					rx_payload.data[1], rx_payload.data[2],
					rx_payload.data[3], rx_payload.data[4],
					rx_payload.data[5], rx_payload.data[6],
					rx_payload.data[7]);
		}
		break;
	}
}

int clocks_start(void)
{
	int err;
	int res;
	struct onoff_manager *clk_mgr;
	struct onoff_client clk_cli;

	clk_mgr = z_nrf_clock_control_get_onoff(CLOCK_CONTROL_NRF_SUBSYS_HF);
	if (!clk_mgr)
	{
		LOG_ERR("Unable to get the Clock manager");
		return -ENXIO;
	}

	sys_notify_init_spinwait(&clk_cli.notify);

	err = onoff_request(clk_mgr, &clk_cli);
	if (err < 0)
	{
		LOG_ERR("Clock request failed: %d", err);
		return err;
	}

	do
	{
		err = sys_notify_fetch_result(&clk_cli.notify, &res);
		if (!err && res)
		{
			LOG_ERR("Clock could not be started: %d", res);
			return res;
		}
	} while (err);

	LOG_DBG("HF clock started");
	return 0;
}

void led_start()
{
	nrf_gpio_cfg_output(LED0);
	nrf_gpio_cfg_output(LED1);
	nrf_gpio_pin_clear(LED0);
}

int esb_initialize(void)
{
	int err;
	/* These are arbitrary default addresses. In end user products
	 * different addresses should be used for each set of devices.
	 */
	uint8_t base_addr_0[4] = {0xE7, 0xE7, 0xE7, 0xE7};
	uint8_t base_addr_1[4] = {0xC2, 0xC2, 0xC2, 0xC2};
	uint8_t addr_prefix[8] = {0xE7, 0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7, 0xC8};

	struct esb_config config = ESB_DEFAULT_CONFIG;

	config.protocol = ESB_PROTOCOL_ESB_DPL;
	config.retransmit_delay = 600;
	config.bitrate = ESB_BITRATE_2MBPS;
	config.event_handler = event_handler;
	config.mode = ESB_MODE_PTX;
	config.selective_auto_ack = true;

	if (check_result("esb_init", esb_init(&config)))
		return -1;

	if (check_result("esb_set_base_address_0", esb_set_base_address_0(base_addr_0)))
		return -1;

	if (check_result("esb_set_base_address_1", esb_set_base_address_1(base_addr_1)))
		return -1;

	if (check_result("esb_set_prefixes", esb_set_prefixes(addr_prefix, ARRAY_SIZE(addr_prefix))))
		return -1;

	return 0;
}

int main(void)
{
	int err;

	LOG_INF("Enhanced ShockBurst ptx sample");

	if (check_result("clocks_start", clocks_start()))
		return 0;

	led_start();

	if (check_result("esb_initialize", esb_initialize()))
		return 0;

	LOG_INF("Initialization complete");
	LOG_INF("Sending test packet");
	ready = true;
	while (1)
	{
		esb_flush_rx();
		esb_flush_tx();
		led_off(LED0);
		led_off(LED1);
		k_sleep(K_SECONDS(1));
		err = esb_write_payload(&tx_payload);
		if (err)
		{
			LOG_ERR("Payload write failed, err %d", err);
		}
		tx_payload.data[1]++;
		k_sleep(K_SECONDS(1));
	}
}
