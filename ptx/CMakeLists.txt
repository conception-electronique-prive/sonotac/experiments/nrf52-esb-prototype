#
# Copyright (c) 2018 Nordic Semiconductor
#
# SPDX-License-Identifier: LicenseRef-Nordic-5-Clause
#
cmake_minimum_required(VERSION 3.20.0)

find_package(Zephyr REQUIRED HINTS $ENV{ZEPHYR_BASE})
project(NONE)

set(COMMON_DIR ${CMAKE_SOURCE_DIR}/../common)
FILE(GLOB app_sources src/*.c)
# NORDIC SDK APP START
target_sources(app PRIVATE ${app_sources}
${COMMON_DIR}/check_result.c
${COMMON_DIR}/esb_configuration.c
${COMMON_DIR}/esb_utils.c
${COMMON_DIR}/initialization.c)
# NORDIC SDK APP END
target_include_directories(app PRIVATE ${COMMON_DIR})
target_compile_options(app PRIVATE -Wno-unused-function)
target_compile_options(app PRIVATE -Wno-unused-variable)
