#include <esb.h>
#include <nrfx_systick.h>
#include <nrfx_uart.h>
#include <string.h>
#include <zephyr/drivers/clock_control.h>
#include <zephyr/drivers/clock_control/nrf_clock_control.h>
#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>

#include "check_result.h"
#include "esb_configuration.h"
#include "initialization.h"

LOG_MODULE_REGISTER(APP, LOG_LEVEL_DBG);

static int handle_discovery() {
  struct esb_payload advertissement = ESB_CREATE_PAYLOAD(0, addr_1[0], addr_1[1], addr_1[2], addr_1[3]);
  int                err            = esb_write_payload(&advertissement);
  if (err) LOG_DBG("%s", "Failed to add advertissement");
  return err;
}

static int handle_command(const struct esb_payload* payload) {
  static uint8_t     status[7] = { 0, 6, 6, 0, 0, 0, 0 };
  struct esb_payload response  = ESB_CREATE_PAYLOAD(1, 0, 6, 6, 0, 0, 0, 0);
  uint8_t            command   = payload->data[0];
  if (command == 2) {  // Play/Pause
    LOG_DBG("%s", "Play/Pause");
    status[0] = status[0] == 0 ? 1 : 0;
  } else if (command == 3) {  // Prec
    LOG_DBG("%s", "Prec");
    status[3] = status[3] == 0 ? 12 : status[3] - 1;
  } else if (command == 4) {  // Suiv
    LOG_DBG("%s", "Suiv");
    status[3] = status[3] == 12 ? 0 : status[3] + 1;
  } else if (command == 5) {  // Vol +
    LOG_DBG("%s", "Vol +");
    status[1] = status[1] == 0 ? 12 : status[1] - 1;
  } else if (command == 6) {  // Vol -
    LOG_DBG("%s", "Vol -");
    status[1] = status[1] == 12 ? 0 : status[1] + 1;
  } else if (command == 7) {  // Gss +
    LOG_DBG("%s", "Gss +");
    status[2] = status[2] == 0 ? 12 : status[2] - 1;
  } else if (command == 8) {  // Gss -
    LOG_DBG("%s", "Gss -");
    status[2] = status[2] == 12 ? 0 : status[2] + 1;
  }
  for (int i = 0; i < 7; ++i) response.data[i] = status[i];
  int err = esb_write_payload(&response);
  if (err) {
    LOG_ERR("%s", "Failed to add status");
    return err;
  }
  return 0;
}

static void on_esb_event(const struct esb_evt* event) {
  switch (event->evt_id) {
    case ESB_EVENT_RX_RECEIVED: LOG_DBG("%s", "ESB EVENT RX RECEIVED"); break;
    case ESB_EVENT_TX_FAILED: LOG_DBG("%s", "ESB EVENT TX FAILED"); break;
    case ESB_EVENT_TX_SUCCESS: LOG_DBG("%s", "ESB EVENT TX SUCCESS"); break;
  }
  // if (event->evt_id != ESB_EVENT_RX_RECEIVED) return;
  // struct esb_payload rx_payload;
  // int                err = esb_read_rx_payload(&rx_payload);
  // if (err) return;
  // if (rx_payload.pipe == 0) {
  //   LOG_DBG("%s", "Replying to discovery");
  //   handle_discovery();
  // } else {
  //   LOG_DBG("%s", "Handling command");
  //   handle_command(&rx_payload);
  // }
}

int main() {
  int err;
  err = init_clock();
  if (err) return err;
  LOG_INF("controller %s %s", __DATE__, __TIME__);

  struct esb_config esb_config = ESB_DEFAULT_CONFIG;
  esb_config.event_handler     = on_esb_event;
  esb_config.mode              = ESB_MODE_PRX;

  init_esb(&esb_config, addr_0, addr_1, addr_prefix);
  check_result("start_rx", esb_start_rx());

  uint32_t start = k_uptime_get_32();
  uint32_t last  = start;
  uint32_t delay = 100;
  while (true) {
    struct esb_payload esb_payload;
    if (esb_read_rx_payload(&esb_payload) == 0) {
      LOG_DBG("Received payload");
      LOG_DBG("Pipe: %d", esb_payload.pipe);
      LOG_DBG("RSSI: %d", esb_payload.rssi);
      LOG_DBG("Size: %d", esb_payload.length);
      LOG_HEXDUMP_DBG(esb_payload.data, esb_payload.length, "Data");
    }
    uint32_t now = k_uptime_get_32();
    if (now - last > delay) {
      last = now;
      printk("Running since %u s\r", last / 1000);
    }
  }
  return 0;
}
